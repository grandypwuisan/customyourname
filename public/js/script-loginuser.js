$(document).ready(function(){
    firebase.auth().onAuthStateChanged(function(user) {
        //Login
        $('.btnLogin').on('click',(e)=> {
            $(".btnLogin").attr('disabled','disabled');
            e.preventDefault();
            if ($('#loginEmail').val() && $('#loginPassword').val()) {
                //login the user
                const data = {
                    email: $('#loginEmail').val(),
                    password: $('#loginPassword').val()
                };
                firebase.auth().signInWithEmailAndPassword(data.email, data.password)
                .then((authData) => {
                    auth = authData;
                    alert("Berhasil Login");
                    if(firebase.auth().currentUser.uid=="PkCd5XXpruRxabj5ioypv3Cne3E2"){
                        $(location).attr('href', base_url+'/home_pengrajin');
                    }else{
                        $(location).attr('href', base_url+'/home');
                    }
                })
                .catch((error) => {
                    console.log("Gagal Login!", error);
                    $(".btnLogin").removeAttr('disabled');
                    alert("gagal login");
                });
            }
            else{
                alert("pastikan semua data terisi");
                $(".btnLogin").removeAttr('disabled');
            }
        });
    });
});