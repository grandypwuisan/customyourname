/*================================================================================
	Item Name: Materialize - Material Design Admin Template
	Version: 4.0
	Author: PIXINVENT
	Author URL: https://themeforest.net/user/pixinvent/portfolio
================================================================================

NOTE:
------
PLACE HERE YOUR OWN JS CODES AND IF NEEDED.
WE WILL RELEASE FUTURE UPDATES SO IN ORDER TO NOT OVERWRITE YOUR CUSTOM SCRIPT IT'S BETTER LIKE THIS. */
$(document).ready(function(){
    //ini untuk atasi bug double click dropdown
    $('select').material_select();
    document.querySelectorAll('.select-wrapper').forEach(t => t.addEventListener('click', e=>e.stopPropagation()));

    //config firebase
    var firebaseConfig = {
        apiKey: "AIzaSyDb4yLINiZybuXakvQg6sjp_c0MtRQH9fQ",
        authDomain: "customyourname-3107.firebaseapp.com",
        databaseURL: "https://customyourname-3107-default-rtdb.firebaseio.com",
        projectId: "customyourname-3107",
        storageBucket: "customyourname-3107.appspot.com",
        messagingSenderId: "660736596647",
        appId: "1:660736596647:web:12a5aa089d21866a7bd982",
        measurementId: "G-YXN66BK7X9"
    };
    firebase.initializeApp(firebaseConfig);
	
});