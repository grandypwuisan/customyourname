$(document).ready(function(){
    $('.modal').modal();
    $("#btnKembali").click(function(){
        window.location.href = base_url+"/home_pengrajin"
    });
    var formatter = new Intl.NumberFormat('in-ID', {
        style: 'currency',
        currency: 'IDR',
      });
    firebase.auth().onAuthStateChanged(function(user) {
        //ambil data produk dari db dan tampilkan ke halaman daftarpesanan
        var id_pesanan = new URLSearchParams(window.location.search).get("id")
        ambildatapesanan(id_pesanan)
	});

    function ambildatapesanan(id){
        firebase.database().ref('pesanan/admin/'+id).once('value').then((snapshot)=>{
            var value = snapshot.val();
            if(snapshot){
                var htmls = "";
                var jumlah = [];
                var listbarang = [];
               for(i=0; i<value.daftar_barang.length;i++){
                   var barang = value.daftar_barang[i]
                   listbarang.push(barang);
                   jumlah.push(barang.jumlah_produk);
                htmls=htmls+'<tr>\
                    <td><img class="gambarDetailProduk"src="'+barang.foto_produk+'"<br><br>' 
                    + barang.nama_produk + '<br>'+barang.jumlah_produk+' produk</td>'
                    +"<td>Rp <b><input style='width:100px;font-size:large' class='harga'type:text value='"+barang.harga_produk+"'></input></b></td>"
                    +"<td><b><input style='width:50px; font-size:large' class='berat'type:text value='"+barang.berat_produk+"'></input></b>gram</td>"
                    +'<td>'+ barang.warna_produk;
                    if(barang.kategori_produk=="kalung"){
                        htmls = htmls + ", "+barang.panjang_kalung;
                    }
                    htmls = htmls+'</td><td><span class="subtotal">'+formatter.format(barang.jumlah_produk*barang.harga_produk)+'</span></td>';
                    if(barang.custom_produk =="ya"){
                        htmls=htmls+"<td><a id='"+i+"'href='#modal1'"
                        +"class='btnLihatDesain btn modal-trigger waves-effect waves-light pink lighten-2'>Lihat Desain</a></td>"
                    }
                    else{
                        htmls=htmls+"<td>bukan barang custom</td>"
                    }
                    htmls = htmls+
                        `<td><a id='`+i+`'class="btnSimpan btn waves-effect waves-light pink lighten-2">
                        Simpan</a></td>
                        </tr>`;
               }
               $('#tabeldaftarbarang').html(htmls);

               //masukkan status pesanan
                if(value.status_pesanan=="baru"){
                    if(value.acc_admin=="ya"){
                        var htmls2="<h5 style='color:red'>Status Pesanan: Menunggu Konfirmasi Pengrajin</h5>"
                        +"<span>Dipesan : </span>"+value.tgl_pemesanan +"<hr>"
                        +'<br><br><br><textarea id="catatanPengrajin" placeholder="Catatan Pesanan" class="materialize-textarea"></textarea>'
                        +'<a class="waves-effect waves-dark btn" id="btnSetuju">Setujui Pesanan</a>&nbsp'
                        +'<a class="waves-effect waves-dark btn" id="tolakPesanan">Tolak Pesanan</a><br><br>';
                    }
                    else{
                        var htmls2="<h5 style='color:red'>Status Pesanan: Menunggu Konfirmasi Admin</h5>"
                        +"<span>Dipesan : </span>"+value.tgl_pemesanan +"<hr>";
                    }
                    $("#statuspesanan").html(htmls2);
                }
                else if (value.status_pesanan == "disetujui"){
                    var htmls2="<h5 style='color:red'>Status Pesanan: Telah disetujui, sedang menunggu pembayaran</h5>"
                    +"<span>Dipesan : </span>"+value.tgl_pemesanan +"<hr>"
                    $("#statuspesanan").html(htmls2);
                }
                else if (value.status_pesanan == "ditolak"){
                    var htmls2="<h5 style='color:red'>Status Pesanan: Ditolak</h5>"
                    +"<span>Dipesan : </span>"+value.tgl_pemesanan +"<hr>"
                    $("#statuspesanan").html(htmls2);
                }
                else if (value.status_pesanan == "dibayar"){
                    var htmls2="<h5 style='color:red'>Status Pesanan: Dibayar, menunggu admin melakukan verifikasi</h5>"
                    +"<span>Dipesan : </span>"+value.tgl_pemesanan +"<hr>"
                    $("#statuspesanan").html(htmls2);
                }
                else if(value.status_pesanan=="diverifikasi"){
                    var htmls2="<h5 style='color:red'>Status Pesanan: Diverifikasi, silahkan memproses pesanan</h5>"
                    +"<span>Dipesan : </span>"+value.tgl_pemesanan +"<hr>"
                    +'<br><a class="waves-effect waves-dark btn" id="btnProses">Proses Pesanan</a><br><br>'
                    $("#statuspesanan").html(htmls2);
                }
                else if(value.status_pesanan=="diproses"){
                    var htmls2="<h5 style='color:red'>Status Pesanan: Diproses, silahkan mengirim pesanan jika telah selesai</h5>"
                    +"<span>Dipesan : </span>"+value.tgl_pemesanan +"<hr>"
                    +'<br><a class="waves-effect waves-dark btn" id="btnKirim">Kirim Pesanan</a><br><br>'
                    $("#statuspesanan").html(htmls2);
                }
                else if(value.status_pesanan=="dikirim"){
                    var htmls2="<h5 style='color:red'>Status Pesanan: Dikirim, jangan lupa memasukkan nomor resi jika telah tersedia</h5>"
                    +"<span>Dipesan : </span>"+value.tgl_pemesanan +"<hr>"
                    $("#statuspesanan").html(htmls2);
                }
                else if(value.status_pesanan=="selesai"){
                    var htmls2="<h5 style='color:red'>Status Pesanan: Selesai</h5>"
                    +"<span>Dipesan : </span>"+value.tgl_pemesanan +"<hr>"
                    $("#statuspesanan").html(htmls2);
                }

               //alamat pesanan
               var htmls3="<h6>Alamat Pengiriman:</h6>"
                    +value.pembeli.nama+"<br>"+value.pembeli.nomorhp+"<br>"+value.pembeli.alamat+", "
                    +value.pembeli.kecamatan+", "+value.pembeli.kota+", "+value.pembeli.provinsi
                    +"<br><br>"+"Kurir :   "+value.kurir+"<br><b>Nomor resi:</b>";
                if(value.status_pesanan!="ditolak"&&value.status_pesanan!="selesai"){
                    htmls3 = htmls3+"<input style='width:100px; margin-left:1%;' id='nomorResi'type:text value='"+value.no_resi+"'></input>"
                    +'&nbsp&nbsp<a class="waves-effect waves-dark btn" id="btninputresi">Input Resi</a><br><br>';
                }
                else{
                    htmls3 = htmls3+"<span>"+value.no_resi+"</span>";
                }
                //catatan dari customer dan admin
                var htmls3=htmls3+"<b>Catatan Admin:</b><br>"+value.catatan_admin+"<br><br>";
                var htmls3=htmls3+"<b>Catatan Customer:</b><br>"+value.catatan+"<br><br>";
                $("#alamatpesanan").html(htmls3);
            }
            //kirim pesanan
            $("#btnKirim").click(function(){
                var ref = firebase.database().ref("pesanan/admin/"+id);
                var ref2 = firebase.database().ref("pesanan/"+value.pembeli.id_user+"/"+id)
                var data = {
                    status_pesanan : "dikirim"
                }
                ref.update(data);
                ref2.update(data);
                alert("Pesanan dikirim, jangan lupa menginputkan nomor resi jika telah tersedia!");
                location.reload();
           });

            //proses
            $("#btnProses").click(function(){
                var ref = firebase.database().ref("pesanan/admin/"+id);
                var ref2 = firebase.database().ref("pesanan/"+value.pembeli.id_user+"/"+id)
                var data = {
                    status_pesanan : "diproses"
                }
                ref.update(data);
                ref2.update(data);
                alert("Pesanan diproses");
                location.reload();
           });

            //setujui pesanan
            $("#btnSetuju").click(function(){
                 var ref = firebase.database().ref("pesanan/admin/"+id);
                 var ref2 = firebase.database().ref("pesanan/"+value.pembeli.id_user+"/"+id)
                 var catatan = $("#catatanPengrajin").val();
                 var data = {
                     status_pesanan : "disetujui",
                     catatan : catatan
                 }
                 ref.update(data);
                 ref2.update(data);
                 alert("Pesanan telah disetujui");
                 location.reload();
            });

            //tolak pesanan
            $("#tolakPesanan").click(function(){
                var catatan = $("#catatanPengrajin").val()
                if(catatan==""){
                    alert("Mohon isi catatan mengenai alasan penolakan");
                }
                else{
                    var ref = firebase.database().ref("pesanan/admin/"+id);
                    var ref2 = firebase.database().ref("pesanan/"+value.pembeli.id_user+"/"+id)
                    ref.child("status_pesanan").set("ditolak");
                    ref.child("catatan").set(catatan);
                    ref2.child("status_pesanan").set("ditolak");
                    ref2.child("catatan").set(catatan);
                    alert("Pesanan telah ditolak");
                    location.reload();
                }
            });

            //input resi kalau status sudah terkirim
            $("#btninputresi").click(function(){
                if(value.status_pesanan=="dikirim"){
                    //baru bisa input resi
                    var noresi = $("#nomorResi").val()
                    var ref = firebase.database().ref("pesanan/admin/"+id);
                    var ref2 = firebase.database().ref("pesanan/"+value.pembeli.id_user+"/"+id)
                    ref.child("no_resi").set(noresi);
                    ref2.child("no_resi").set(noresi);
                    alert("nomor resi diupdate");
                }
                else{
                    alert("input nomor resi setelah melakukan pengiriman");
                }
            });

            $(".btnSimpan").click(function(){
                if(value.status_pesanan=="baru"){
                    var index = $(this).attr('id');
                    var harga = $(".harga")[index].value;
                    var berat = $(".berat")[index].value;
                    //update data di firebase
                    var dataupdate = {
                        harga_produk : harga,
                        berat_produk : berat
                    }
                    firebase.database().ref("pesanan/admin/"+id+"/daftar_barang/"+index).update(dataupdate)
    
                    //update di db user juga
                    firebase.database().ref("pesanan/"+value.pembeli.id_user+"/"+id+"/daftar_barang/"+index).update(dataupdate)
    
                    //update subtotal harga
                    var subtotal = $(".subtotal")[index];
                    console.log(subtotal);
                    subtotal.innerHTML=harga*jumlah[index]
    
                    alert("detail pesanan diupdate");
                }
                else{
                    alert("pesanan sudah terkonfirmasi, tidak dapat diubah lagi");
                }
            });

            $(".btnLihatDesain").click(function(){
                var index = $(this).attr('id');
                var desain = listbarang[index].desain;
                var htmls="";
                if(desain.jenis=="clipart"){
                    for(var i=0; i<desain.list_url_clipart.length; i++){
                        htmls=htmls+"<img src='"+desain.list_url_clipart[i]+"'>";
                    }
                }
                else{
                    htmls=htmls+"<img style='max-width:500px;max-height:500px'src='"+desain.url_gambar+"'>";
                }

                $("#desain").html(htmls);
            });

        }).catch((error)=>{
            console.log(error);
        });
    }

    function update_pesanan(idPesanan,indexBarang){
        alert("tes")
    }
});