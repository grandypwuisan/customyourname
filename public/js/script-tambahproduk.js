$(document).ready(function(){
    firebase.auth().onAuthStateChanged(function(user) {
        //ini untuk atasi bug double click dropdown
        $('select').material_select();
        document.querySelectorAll('.select-wrapper').forEach(t => t.addEventListener('click', e=>e.stopPropagation()));

        //add data baru
		$('#btnTambahProduk').on('click', function (e) {
            var produkID = "produkid";
            var namaproduk = $("#namaproduk").val();
            var deskripsiproduk = $("#deskripsiproduk").val();
            var kategoriproduk = $("#kategoriproduk").val();
            var hargaproduk = parseInt($("#hargaproduk").val());
            var beratproduk = $("#beratproduk").val();
            var customproduk = $("#customproduk").val();
            if(namaproduk&&deskripsiproduk&&kategoriproduk&&hargaproduk&&beratproduk&&customproduk){
                if($("#fotoProduk")[0].files.length>0){
                    var lastIndex = 0;
                    // Get Data
                    firebase.database().ref('produk/').get().then((snapshot)=>{
                        if(snapshot.exists()){
                            lastIndex = snapshot.numChildren();
                        }
                        //tambahdata
                        produkID = lastIndex + 1;
                        firebase.database().ref('produk/' + produkID).set({
                            namaproduk : namaproduk,
                            deskripsiproduk : deskripsiproduk,
                            kategoriproduk : kategoriproduk,
                            hargaproduk : hargaproduk,
                            beratproduk : beratproduk,
                            statusproduk :"aktif",
                            idproduk : produkID,
                            customproduk : customproduk,
                            gambarproduk:""
                        }).then(()=>{
                            console.log(namaproduk+kategoriproduk+deskripsiproduk+hargaproduk+","+beratproduk+","+produkID);
                            //upload gambar
                            var imageFiles = $("#fotoProduk")[0].files;
                            for(var i=0; i<imageFiles.length; i++){
                                uploadImage(imageFiles[i],produkID);
                            }
                            alert("Berhasil Menambah Produk Baru");
                            $(".formtambahproduk input").val("");
                            $("#deskripsiproduk").val("");
                        }).catch((error)=>{
                            console.log(error);
                            alert("Gagal menambnah produk\n"+error)
                        });
                    }).catch((error)=>{
                        console.log(error);
                        alert("Gagal menambnah produk\n"+error)
                    });
                }
                else{
                    alert("produk harus mempunyai minimal 1 gambar");
                }
            }
            else{
                alert("pastikan semua data terisi!");
            }

            
        });
        //promise untuk upload foto produk
        function uploadImage(imageFile,produkID) {
            return new Promise(function (resolve, reject) {
                var rootRef = firebase.storage().ref();
                var imageRef = rootRef.child('produk');
                console.log(produkID.toString());
                var storageRef = imageRef.child('/'+produkID+'/'+imageFile.name)
                //Upload file
                storageRef.put(imageFile).then((snapshot) => {
                    snapshot.ref.getDownloadURL().then(function(downloadURL) {
                        //console.log("File available at", downloadURL);
                        //setelah selesai upload, data urlnya dimasukkan ke realtime database
                        //krn storage cuma bisa ambil gambar kalo namanya ditau
                        var gambarRef = firebase.database().ref('produk/' + produkID+"/gambarproduk")
                        var gambarBaru = gambarRef.push();
                        gambarBaru.set({
                            "url":downloadURL
                        }).then(()=>{
                            console.log(downloadURL);
                        }).catch((error)=>{
                            console.log(error);
                        });
                    });
                });
                //Update progress bar
                /*task.on('state_changed',
                    function progress(snapshot){
                        var percentage = snapshot.bytesTransferred / snapshot.totalBytes * 100;
                    },
                    function error(err){
                        console.log(error);
                    },
                    function complete(){
                        var downloadURL = snapshot.downloadURL;
                        console.log(downloadURL);
                        
                    }
                );*/
            });
        }
	});
});