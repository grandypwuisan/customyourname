$(document).ready(function(){
    var xValues = ["Desain Tulisan", "Desain Clipart", "Upload Desain"];
    var yValues = [0, 0, 0];
    var barColors = ["pink", "yellow","blue"];
    firebase.auth().onAuthStateChanged(function(user) {
        loadData();
    });
    function loadData(){
        firebase.database().ref('desain').once('value').then((snapshot)=>{
            if(snapshot.numChildren()>0){  
                var value = snapshot.val();
                $.each(value, function (index, value) {//loop semua user di desain
                    $.each(value, function (index, value) {
                        //ini loop daftar desain dari 1 user
                        if(value.jenis=="tulisan"){
                            yValues[0]=yValues[0]+1;
                        }
                        else if(value.jenis=="clipart"){
                            yValues[1]=yValues[1]+1;
                        }
                        else{
                            yValues[2]=yValues[2]+1;
                        }
                    });
                });

                console.log(yValues);

                //buat chartnya
                new Chart("chartDesain", {
                    type: "bar",
                    data: {
                      labels: xValues,
                      datasets: [{
                        label:["data desain"],
                        backgroundColor: barColors,
                        data: yValues
                      }]
                    },
                    options: {
                      legend: {display: false},
                      title: {
                        display: false,
                        text: "Penggunaan Jenis Desain"
                      }
                    }
                  });
            }
        });
    }
});