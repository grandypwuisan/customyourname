$(document).ready(function(){
    var nama = [];
    var jumlah_pesan = [];
    var barColors = ["pink", "yellow","blue","green","orange"];
    firebase.auth().onAuthStateChanged(function(user) {
        loadData();
    });
    function loadData(){
        firebase.database().ref('pesanan').orderByChild("jumlah_pesanan").limitToLast(5).once('value').then((snapshot)=>{
            if(snapshot.numChildren()>0){  
                var value = snapshot.val();
                var jumlah_pembeli = snapshot.numChildren();
                console.log(jumlah_pembeli);
                var ctr=0;
                $.each(value, function (index, value) {
                    ctr++;
                    if(ctr < jumlah_pembeli){
                        nama.push(value[Object.keys(value)[0]].pembeli.nama);
                        jumlah_pesan.push(value.jumlah_pesanan);
                    }
                });
                console.log(nama);

                //buat chartnya
                new Chart("chartPembeli", {
                    type: "pie",
                    data: {
                      labels: nama,
                      datasets: [{
                        label:["Data Pemesan"],
                        backgroundColor: barColors,
                        data: jumlah_pesan
                      }]
                    },
                    options: {
                      legend: {display: false},
                      title: {
                        display: false,
                        text: "Penggunaan Jenis Desain"
                      },
                      radius:"70%"
                    }
                  });
            }
        });
    }
});