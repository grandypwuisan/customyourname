$(document).ready(function(){
    $('#nav_daftarpesanan').addClass('active');
    firebase.auth().onAuthStateChanged(function(user) {
        //ambil data produk dari db dan tampilkan ke halaman daftarpesanan
        $("#btnbaru").click(function(){
            ambildatapesanan("baru")
            //refreshBackground();
            //$("#baru").css('background-color','#d93883');
        });
        $("#btndisetujui").click(function(){
            //refreshBackground();
            ambildatapesanan("disetujui");
            //$("#disetujui").css('background-color','#d93883')
        });
        $("#btnditolak").click(function(){
            //refreshBackground();
            ambildatapesanan("ditolak");
            //$("#ditolak").css('background-color','#d93883')
        });
        $("#btndiproses").click(function(){
            //refreshBackground();
            ambildatapesanan("diproses");
            //$("#diproses").css('background-color','#d93883')
        });
        $("#btndibayar").click(function(){
            //refreshBackground();
            ambildatapesanan("dibayar")
            //$("#dibayar").css('background-color','#d93883')
        });
        $("#btnVerifikasi").click(function(){
            //refreshBackground();
            ambildatapesanan("diverifikasi")
            //$("#diverifikasi").css('background-color','#d93883')
        });
        $("#btndikirim").click(function(){
            //refreshBackground();
            ambildatapesanan("dikirim");
            //$("#dikirim").css('background-color','#d93883')
        });
        $("#btnselesai").click(function(){
            //refreshBackground();
            ambildatapesanan("selesai")
            //$("#selesai").css('background-color','#d93883')
        });

        hitungJumlahPesanan("selesai");
        hitungJumlahPesanan("baru");
        hitungJumlahPesanan("disetujui");
        hitungJumlahPesanan("ditolak");
        hitungJumlahPesanan("diproses");
        hitungJumlahPesanan("dibayar");
        hitungJumlahPesanan("diverifikasi");
        hitungJumlahPesanan("dikirim");
        ambildatapesanan("baru")
        //$("#baru").css('background-color','#d93883')

        $("#btnCariPesanan").click(function(){
            var nama = $("#namaproduk").val();
            if(nama==""){
                
            }
            else{
                firebase.database().ref('pesanan/admin').orderByChild("pembeli/nama").equalTo(nama)
                .once('value').then((snapshot)=>{
                    if(snapshot.numChildren()>0){
                        var value = snapshot.val();
                        var htmls = "";
                        var ctr=0;
                        $.each(value, function (index, value) {
                            if (value) {
                                    htmls=htmls+'<tr>\
                                    <td>' + value.daftar_barang.length + ' Produk </td>\
                                    <td> ' + value.pembeli.nama+ ', '+value.pembeli.kota+'</td>\
                                    <td>'+value.status_pesanan+'</td>';
            
                                    var key = Object.keys(snapshot.val())[ctr];
                                    htmls = htmls+
                                        `<td><a href="`+base_url+'/detailpesanan?id='+key+`" class="btn waves-effect waves-light pink lighten-2">
                                        Detail</a></td>
                                        </tr>`;
                            }
                            ctr++
                        });
                        $('#tabeldaftarpesanan').html(htmls);
                        $('#statuspesanan').html('Pesanan '+nama);
                    }
                    else{
                        $('#tabeldaftarpesanan').html("Tidak ada pesanan...");
                        $('#statuspesanan').html('Pesanan '+nama);
                    }
                }).catch((error)=>{
                    console.log(error);
                });
            }
        });
	});

    function ambildatapesanan(status){
        firebase.database().ref('pesanan/admin').orderByChild("status_pesanan").equalTo(status)
        .once('value').then((snapshot)=>{
            if(snapshot.numChildren()>0){
                var value = snapshot.val();
                var htmls = "";
                var ctr=0;
                $.each(value, function (index, value) {
                    var bisa = true;
                    if (value) {
                            htmls=htmls+'<tr>\
                            <td>' + value.daftar_barang.length + ' Produk </td>\
                            <td> ' + value.pembeli.nama+ ', '+value.pembeli.kota+'</td>';
                            if(status=="baru"){
                                if(value.acc_admin=="ya"){
                                    htmls = htmls+'<td>Sudah Acc</td>';
                                }
                                else{
                                    htmls = htmls+'<td>Belum Acc</td>';
                                }
                            }
                            else{
                                htmls = htmls+'<td>'+value.status_pesanan+'</td>';
                            }
                            var key = Object.keys(snapshot.val())[ctr];
                            htmls = htmls+
                                `<td><a href="`+base_url+'/detailpesanan?id='+key+`" class="btn waves-effect waves-light pink lighten-2">
                                Detail</a></td>
                                </tr>`;
                    }
                    ctr++
                });
                $('#tabeldaftarpesanan').html(htmls);
                $('#statuspesanan').html('Pesanan '+status);
            }
            else{
                $('#tabeldaftarpesanan').html("Tidak ada pesanan...");
                $('#statuspesanan').html('Pesanan '+status);
            }
        }).catch((error)=>{
            console.log(error);
        });
    }

    // function refreshBackground(){
    //     $("#baru").css('background-color','');
    //     $("#disetujui").css('background-color','');
    //     $("#ditolak").css('background-color','');
    //     $("#diproses").css('background-color','');
    //     $("#dibayar").css('background-color','');
    //     $("#diverifikasi").css('background-color','');
    //     $("#diverifikasi").css('background-color','');
    //     $("#dikirim").css('background-color','');
    //     $("#selesai").css('background-color','');
    // }

    function hitungJumlahPesanan(status){
        firebase.database().ref('pesanan/admin').orderByChild("status_pesanan").equalTo(status)
        .once('value').then((snapshot)=>{
            var jumlahPesanan = snapshot.numChildren();
            $('#'+status).append(''+jumlahPesanan+' ');
        });
    }
});