$(document).ready(function(){
    var idTerakhir = 0;
    var idAwal =0;
    var halaman = 0;
    var jumlahhalaman=0;
    var formatter = new Intl.NumberFormat('in-ID', {
        style: 'currency',
        currency: 'IDR',
      });
    firebase.auth().onAuthStateChanged(function(user) {
        //ambil data produk dari db dan tampilkan ke halaman daftarproduk
        loadData();
        hitungjumlahhalaman();

        $('#btnkanan').click(function(){
                loadNext();
        });
        $('#btnkiri').click(function(){
            if(idTerakhir>6){
                loadPrevious();
            }
        });

        //button cari user
        $("#btnCariProduk").click(function(){
            var nama = $("#namaproduk").val();
            if(nama==""){
                loadData();
            }
            else{
                firebase.database().ref('produk').orderByChild("namaproduk").equalTo(nama)
                .once('value').then((snapshot)=>{
                    if(snapshot.numChildren()>0){
                        var value = snapshot.val();
                        var htmls = "";
                        var ctr=0;
                        $.each(value, function (index, value) {
                            if (value) {
                                var harga = formatter.format(value.hargaproduk);
                                htmls=htmls+'<tr>\
                                <td>' + value.namaproduk + '</td>\
                                <td>' + harga+ '</td>';
                                var aktif = "checked";
                                if(value.statusproduk!="aktif"){
                                    aktif="";
                                }
                                htmls = htmls+
                                    `<td><div class="switch">
                                    <label>
                                    <input `+aktif+` class="onoff"type="checkbox"value="`+value.idproduk+`">
                                    <span class="lever"></span>
                                    </label>
                                </div></td>\
                                <td><a href="`+base_url+'/detailproduk?id='+value.idproduk+`" class="btn waves-effect waves-light pink lighten-2">
                                Detail</a></td>
                                    </tr>`;
                            }
                            lastIndex = index;
                        });
                        $('#tabeldaftarproduk').html(htmls);
                        $(".onoff").click(function(){
                            var idproduk = this.value;
                            var status = "nonaktif";
                            if(this.checked){
                                status = "aktif";
                            }
                            //update status produknya
                            var data = {
                                statusproduk : status
                            };
                            firebase.database().ref("/produk/"+idproduk).update(data).then(()=>{
                                alert("Status Produk Diubah Menjadi "+status);
                            }).catch((error)=>{
                                alert(error);
                            });
                        });
                    }
                    else{
                        $('#tabeldaftarproduk').html("Tidak dapat menemukan produk");
                    }
                }).catch((error)=>{
                    console.log(error);
                });
            }
        });

        function hitungjumlahhalaman(){
            firebase.database().ref('/produk').once('value').then((snapshot)=>{
                jumlahhalaman = Math.ceil(snapshot.numChildren()/5);
                updateHalaman();
            });
        }

        function updateHalaman(){
            $('#jumHalaman').html('<i>halaman '+(halaman+1)+' / '+jumlahhalaman+'</i>')
        }

        function loadData(){
            firebase.database().ref('produk/').limitToFirst(6).once('value').then((snapshot)=>{
                var value = snapshot.val();
                var htmls = "";
                console.log(value);
                ctr=0;
                $.each(value, function (index, value) {
                    if (value) {
                        if(ctr<5){
                            var harga = formatter.format(value.hargaproduk);
                            htmls=htmls+'<tr>\
                            <td>' + value.namaproduk + '</td>\
                            <td>' + harga+ '</td>';
                            var aktif = "checked";
                            if(value.statusproduk!="aktif"){
                                aktif="";
                            }
                            htmls = htmls+
                                `<td><div class="switch">
                                <label>
                                  <input `+aktif+` class="onoff"type="checkbox"value="`+value.idproduk+`">
                                  <span class="lever"></span>
                                </label>
                              </div></td>\
                              <td><a href="`+base_url+'/detailproduk?id='+value.idproduk+`" class="btn waves-effect waves-light pink lighten-2">
                              Detail</a></td>
                                </tr>`;
                        }
                        else if(ctr==5){
                            idAwal = value.idproduk;
                            idTerakhir = value.idproduk;
                        }
                        ctr++;
                    }
                    lastIndex = index;
                });
                updateHalaman();
                $('#tabeldaftarproduk').html(htmls);
                $(".onoff").click(function(){
                    var idproduk = this.value;
                    var status = "nonaktif";
                    if(this.checked){
                        status = "aktif";
                    }
                    //update status produknya
                    var data = {
                        statusproduk : status
                    };
                    firebase.database().ref("/produk/"+idproduk).update(data).then(()=>{
                        alert("Status Produk Diubah Menjadi "+status);
                    }).catch((error)=>{
                        alert(error);
                    });
                });
            }).catch((error)=>{
                console.log(error);
            });
        }

        function loadNext(){
            firebase.database().ref('produk/').orderByChild('idproduk').startAt(idTerakhir)
            .limitToFirst(6).once('value').then((snapshot)=>{
                var value = snapshot.val();
                console.log(value);
                var htmls = "";
                if(snapshot.numChildren()>0){
                    if(halaman+1<jumlahhalaman)
                    {
                        halaman = halaman+1;
                    }
                    var ctr=0;
                    idAwal = idTerakhir;
                    $.each(value, function (index, value) {
                        if(value){
                            if(ctr<5){
                                var harga = formatter.format(value.hargaproduk);
                                htmls=htmls+'<tr>\
                                <td>' + value.namaproduk + '</td>\
                                <td>' + harga+ '</td>';
                                var aktif = "checked";
                                if(value.statusproduk!="aktif"){
                                    aktif="";
                                }
                                htmls = htmls+
                                    `<td><div class="switch">
                                    <label>
                                    <input `+aktif+` class="onoff"type="checkbox"value="`+value.idproduk+`">
                                    <span class="lever"></span>
                                    </label>
                                </div></td>\
                                <td><a href="`+base_url+'/detailproduk?id='+value.idproduk+`" class="btn waves-effect waves-light pink lighten-2">
                                Detail</a></td>
                                    </tr>`;
                            }
                            else{
                                idTerakhir = value.idproduk;
                            }
                            lastIndex = index;
                            ctr++;
                        }
                    });
                    updateHalaman();
                    $('#tabeldaftarproduk').html(htmls);
                    $(".onoff").click(function(){
                        var idproduk = this.value;
                        var status = "nonaktif";
                        if(this.checked){
                            status = "aktif";
                        }
                        //update status produknya
                        var data = {
                            statusproduk : status
                        };
                        firebase.database().ref("/produk/"+idproduk).update(data).then(()=>{
                            alert("Status Produk Diubah Menjadi "+status);
                        }).catch((error)=>{
                            alert(error);
                        });
                    });
                }
            }).catch((error)=>{
                console.log(error);
            });
        }

        function loadPrevious(){
            firebase.database().ref('produk/').orderByChild('idproduk').endAt(idAwal)
            .limitToLast(6).once('value').then((snapshot)=>{
                var value = snapshot.val();
                var htmls = "";
                console.log(value);
                if(snapshot.numChildren()>0){
                    halaman=halaman-1;
                    var ctr=0;
                    $.each(value, function (index, value) {
                        if(value){
                            if(ctr<5){
                                if(ctr==0){
                                    idTerakhir = idAwal;
                                    idAwal = value.idproduk;
                                    console.log("id Awal : "+idAwal+"idTerakhir : "+idTerakhir);
                                }
                                var harga = formatter.format(value.hargaproduk);
                                htmls=htmls+'<tr>\
                                <td>' + value.namaproduk + '</td>\
                                <td>' +harga+ '</td>';
                                var aktif = "checked";
                                if(value.statusproduk!="aktif"){
                                    aktif="";
                                }
                                htmls = htmls+
                                    `<td><div class="switch">
                                    <label>
                                    <input `+aktif+` class="onoff"type="checkbox"value="`+value.idproduk+`">
                                    <span class="lever"></span>
                                    </label>
                                </div></td>\
                                <td><a href="`+base_url+'/detailproduk?id='+value.idproduk+`" class="btn waves-effect waves-light pink lighten-2">
                                Detail</a></td>
                                    </tr>`;
                            }
                            lastIndex = index;
                            ctr++;
                        }
                    });
                    updateHalaman();
                    $('#tabeldaftarproduk').html(htmls);
                    $(".onoff").click(function(){
                        var idproduk = this.value;
                        var status = "nonaktif";
                        if(this.checked){
                            status = "aktif";
                        }
                        //update status produknya
                        var data = {
                            statusproduk : status
                        };
                        firebase.database().ref("/produk/"+idproduk).update(data).then(()=>{
                            alert("Status Produk Diubah Menjadi "+status);
                        }).catch((error)=>{
                            alert(error);
                        });
                    });
                }
            }).catch((error)=>{
                console.log(error);
            });
        }
	});
});