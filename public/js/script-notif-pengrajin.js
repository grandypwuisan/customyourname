$(document).ready(function(){
    firebase.auth().onAuthStateChanged(function(user) {
        firebase.database().ref('pesanan/admin').orderByChild("status_pesanan").equalTo("baru")
        .once('value').then((snapshot)=>{
            if(snapshot){
                var value = snapshot.val();
                var htmls = "";
                var ctr=0;
                $.each(value, function (index, value) {
                    htmls = htmls+"<li><a href='"+base_url+"/detailPesananPengrajin?id="+Object.keys(snapshot.val())[ctr]+"'>Ada pesanan baru dari "+value.pembeli.nama+"</a></li>";
                    ctr++
                });
                $('.notification-badge').html(ctr);
                $("#notifications-dropdown").append(htmls)
            }
            else{
                $('#tabeldaftarpesanan').html("Tidak ada hasil...");
            }
        }).catch((error)=>{
            console.log(error);
        });
    });
});