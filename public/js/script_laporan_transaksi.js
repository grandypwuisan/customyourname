$(document).ready(function(){
    var halaman = 0;
    var max = 5;
    var tgl_awal;
    var tgl_akhir;
    var data;
    firebase.auth().onAuthStateChanged(function(user) {
        loadData();
    });

    

    function loadData(){
        firebase.database().ref('pesanan/admin').orderByChild("status_pesanan").equalTo("selesai")
            .once('value').then((snapshot)=>{
                if(snapshot.numChildren()>0){
                    var value = snapshot.val();
                    console.log(value);
                    $("#btnTerapkan").click(function(){
                        tgl_awal = $("#tgl_awal").val();
                        tgl_akhir = $("#tgl_akhir").val();
                        if(tgl_awal==""||tgl_akhir==""){
                            alert("silahkan pilih tanggal terlebih dahulu");
                        }
                        else{
                            filterData(value,new Date(tgl_awal),new Date(tgl_akhir));
                        }
                    });

                    function filterData(value,tglAwal,tglAkhir){
                        halaman=0;
                        data=[];
                        htmls="";
                        var ctr=0;
                        $.each(value, function (index, value) {
                            if (value) {
                                    var tglPesan = new Date(value.tgl_pemesanan);
                                    tglAwal.setHours(0,0,0,0);
                                    tglAkhir.setHours(0,0,0,0);
                                   // console.log(tglPesan+", "+tglAwal+", "+tglAkhir);
                                    if(tglPesan<=tglAkhir&&tglPesan>=tglAwal){
                                        data.push(value);
                                        ctr++
                                    }
                            }
                        });
                        var jsonData = JSON.stringify(data);
                        if(ctr==0){
                            htmls = "<br><i>Tidak ada pesanan pada periode tersebut</i>";
                        }
                        else{
                            for(var i=(halaman*max); i<((halaman+1)*max); i++){
                                if(i<data.length){
                                    htmls=htmls+'<tr>\
                                    <td>' + data[i].daftar_barang.length + ' Produk </td>\
                                    <td> ' + data[i].pembeli.nama+ ', '+data[i].pembeli.kota+'</td>\
                                    <td>'+data[i].status_pesanan+'</td>\
                                    <td>'+data[i].tgl_pemesanan+'</td>';
                                }
                            }
                        }
                        $('#tabeldaftarpesanan').html(htmls);
                        $('#dataprint').val(jsonData);
                    }

                    $("#btnkanan").click(function(){
                        if(halaman+1<(Math.floor(data.length/max)+1)){
                            halaman++;
                            pindahHalaman(data,halaman);
                        }
                    });
            
                    $("#btnkiri").click(function(){
                        if(halaman>0){
                            halaman--;
                            pindahHalaman(data,halaman);
                        }
                    });
                }
            }).catch((error)=>{
                console.log(error);
            })
    }

    function pindahHalaman(data,halaman){
        var htmls="";
        for(var i=(halaman*max); i<((halaman+1)*max); i++){
            if(i<data.length){
                htmls=htmls+'<tr>\
                <td>' + data[i].daftar_barang.length + ' Produk </td>\
                <td> ' + data[i].pembeli.nama+ ', '+data[i].pembeli.kota+'</td>\
                <td>'+data[i].status_pesanan+'</td>\
                <td>'+data[i].tgl_pemesanan+'</td>';
            }
        }
        $('#tabeldaftarpesanan').html(htmls);
    }
});