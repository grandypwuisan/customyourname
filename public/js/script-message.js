$(document).ready(function(){
    $(".tulisChat").hide()
    firebase.auth().onAuthStateChanged(function(user) {
        var idUseryangDichat ="";
        //ambil data chat
        firebase.database().ref('pesan/').on('value',(snapshot)=>{
            //alert(idUseryangDichat);
            var value = snapshot.val();
            var htmls = "<h5>Kotak Pesan</h5><hr>";
            var ctr=0;
            $.each(value, function (index, value) {
                if (value) {
                    var key = Object.keys(snapshot.val())[ctr];
                    htmls=htmls+"<div class='UserChat' id="+key+">"+value.nama_user+"</div>";
                }
                ctr++;
            });
            $("#kolomDaftarChat").html(htmls);

            //append kalo sdh ada chat yg terbuka
            if(idUseryangDichat!=""){
                var data = snapshot.child(idUseryangDichat).child("list_chat").val();
                var htmls2="";
                $.each(data, function (index, value) {
                    if (value) {
                        if(value.id_pengirim == idUseryangDichat){
                            //orang yg kirim
                            htmls2 = htmls2+value.tanggal+"<br>";
                            htmls2=htmls2+"<div class='chatsTerima'>"+value.pesan+"</div><br>"+value.jam+"<br><br><br><br><br><br>";
                            firebase.database().ref('/pesan/'+idUseryangDichat).child("list_chat").child(value.id_chat).
                            child("status").set("read").catch((error)=>{
                                console.log(error+user.uid);
                            });
                        }
                        else{
                            //customyourname yang kirim
                            htmls2 = htmls2+"<div class=containerKirim>"+value.tanggal+"<br>";
                            htmls2=htmls2+"<div class='chatsKirim'>"+value.pesan+"</div>";
                            htmls2=htmls2+value.jam;
                            if(value.status =="read"){
                                htmls2 = htmls2+"  read"
                            }
                            htmls2=htmls2+"</div><br><br><br><br><br><br>";
                        }
                    }
                });
                $("#isiChat").html(htmls2);
                $("#isiChat").scrollTop = $("#isiChat").scrollHeight
            }

            //onclick usernya, tampilkan chat
            $(".UserChat").click(function(){
                var idUserChat = $(this).attr("id");
                idUseryangDichat = idUserChat;
                //$("#idYangDichat").val(idUserChat);
                //console.log(snapshot.child(idUserChat).child("list_chat").val());
                var data = snapshot.child(idUserChat).child("list_chat").val();
                var htmls2="";
                var namaada = false;
                $.each(data, function (index, value) {
                    if (value) {
                        if(value.id_pengirim == idUserChat){
                            //orang yg kirim
                            if(namaada==false){
                                $("#namayangaktif").html("<b><h5 class='namaaktif'>"+value.pengirim+"</h5></b><hr>");
                                namaada = true;
                            }
                            htmls2 = htmls2+value.tanggal+"<br>";
                            htmls2=htmls2+"<div class='chatsTerima'>"+value.pesan+"</div><br>"+value.jam+"<br><br><br><br><br><br>";
                            firebase.database().ref('/pesan/'+idUseryangDichat).child("list_chat").child(value.id_chat).
                            child("status").set("read").catch((error)=>{
                                console.log(error+user.uid);
                            });
                        }
                        else{
                            //customyourname yang kirim
                            htmls2 = htmls2+"<div class=containerKirim>"+value.tanggal+"<br>";
                            htmls2=htmls2+"<div class='chatsKirim'>"+value.pesan+"</div>";
                            htmls2=htmls2+value.jam;
                            if(value.status =="read"){
                                htmls2 = htmls2+"  read"
                            }
                            htmls2=htmls2+"</div><br><br><br><br><br><br>";
                        }
                    }
                });
                $("#isiChat").html(htmls2);
                $(".tulisChat").show()
                $("#isiChat").scrollTop(($("#isiChat").height()+3000));
            });
        });

        $("#btnKirimPesan").click(function(){
            var chat = $("#pesanDitulis").val();
            var date = new Date();
            var tgl = date.getDate().toString().padStart(2, '0')+'/'+(date.getMonth()+1).toString().padStart(2, '0')+"/"+date.getFullYear() ;
            var jam = date.getHours().toString().padStart(2, '0')+":"+date.getMinutes().toString().padStart(2, '0');
            if(chat!=""){
                //kirim ke database
                var ref = firebase.database().ref('pesan/' + idUseryangDichat+'/list_chat').push()
                //var key=""
                //ref.then((snap)=>{key = snap.key})
                ref.set({
                    id_pengirim: user.uid,
                    pengirim:"customyourname.id",
                    pesan: chat,
                    status:"sent",
                    id_chat: ref.key,
                    jam:jam,
                    tanggal:tgl
                }).catch((error)=>{
                    console.log(error);
                    alert("Gagal  mengirim chat");
                });
            }
        });
    })
})