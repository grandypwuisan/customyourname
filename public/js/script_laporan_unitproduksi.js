$(document).ready(function(){
    var halaman = 0;
    var max = 5;
    var tgl_awal;
    var tgl_akhir;
    var data;
    var datafilter;
    firebase.auth().onAuthStateChanged(function(user) {
        loadData();
    });
    function loadData(){
        firebase.database().ref('produk_terjual').once('value').then((snapshot)=>{
                if(snapshot.numChildren()>0){
                    var value = snapshot.val();
                    $("#btnTerapkan").click(function(){
                        tgl_awal = $("#tgl_awal").val();
                        tgl_akhir = $("#tgl_akhir").val();
                        if(tgl_awal==""||tgl_akhir==""){
                            alert("silahkan pilih tanggal terlebih dahulu");
                        }
                        else{
                            filterData(value,new Date(tgl_awal),new Date(tgl_akhir));
                        }
                    });

                    function filterData(value,tglAwal,tglAkhir){
                        halaman=0;
                        data=[];
                        htmls="";
                        var ctr=0;
                        $.each(value, function (index, value) {
                            if (value) {
                                    var tglPesan = new Date(index);
                                    tglAwal.setHours(0,0,0,0);
                                    tglAkhir.setHours(0,0,0,0);
                                    if(tglPesan>=tglAwal){
                                        console.log(tglPesan+", "+tglAwal+", "+tglAkhir);
                                        if(tglPesan<=tglAkhir){
                                            //data.push(value);
                                            console.log(value);
                                            $.each(value, function (index, value) {
                                                if(value){
                                                    if(data[value.idproduk.toString()]){
                                                        data[value.idproduk.toString()].jumlah = parseInt(data[value.idproduk.toString()].jumlah)+parseInt(value.jumlah);
                                                    }else{
                                                        data[value.idproduk.toString()] = value;
                                                    }
                                                }
                                            });
                                            ctr++
                                        }
                                    }
                            }
                        });
                        //masukkan data yg sdh filter
                        datafilter = [];
                        $.each(data, function (index, value) {
                            if(value){
                                datafilter.push(value);
                            }
                        });
                        var jsonData = JSON.stringify(datafilter);
                        if(ctr==0){
                            htmls = "<br><i>Tidak ada pesanan pada periode tersebut</i>";
                        }
                        else{
                            for(var i=(halaman*max); i<((halaman+1)*max); i++){
                                if(i<datafilter.length){
                                    htmls=htmls+'<tr>\
                                    <td>' + datafilter[i].nama + '</td>\
                                    <td> ' + datafilter[i].kategori+'</td>\
                                    <td>'+datafilter[i].jumlah+' produk </td>';
                                }
                            }
                        }
                        $('#tabeldaftarpesanan').html(htmls);
                        $('#dataprint').val(jsonData);
                    }

                    $("#btnkanan").click(function(){
                        if(halaman+1<(Math.floor(datafilter.length/max)+1)){
                            halaman++;
                            pindahHalaman(datafilter,halaman);
                        }
                    });
            
                    $("#btnkiri").click(function(){
                        if(halaman>0){
                            halaman--;
                            pindahHalaman(datafilter,halaman);
                        }
                    });
                }
            }).catch((error)=>{
                console.log(error);
            })
    }

    function pindahHalaman(data,halaman){
        var htmls="";
        for(var i=(halaman*max); i<((halaman+1)*max); i++){
            if(i<data.length){
                htmls=htmls+'<tr>\
                <td>' + data[i].nama + '</td>\
                <td> ' + data[i].kategori+'</td>\
                <td>'+data[i].jumlah+' produk </td>';
            }
        }
        $('#tabeldaftarpesanan').html(htmls);
    }
});