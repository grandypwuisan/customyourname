$(document).ready(function(){
    $('.modal').modal();
    firebase.auth().onAuthStateChanged(function(user) {
        //masukkan info dari firebase ke textarea
        firebase.database().ref("/informasi/deskripsitoko").once('value').then((snapshot)=>{
          if(snapshot.exists()){
            $("#deskripsitoko").val(snapshot.val());
          }
        }).catch(function(error){
          alert(error);
        });

        firebase.database().ref("/informasi/deskripsibahan").once('value').then((snapshot)=>{
          if(snapshot.exists()){
            $("#deskripsibahan").val(snapshot.val());
          }
        }).catch(function(error){
          alert(error);
        });

        //FAQ==========================================
        var lastIndex = 0;
        //ambil data pertanyaan
        firebase.database().ref('informasi/faq').on('value', function (snapshot) {
          var value = snapshot.val();
          var htmls = [];
          $.each(value, function (index, value) {
              if (value) {
                  htmls.push('<tr>\
                  <td>' + value.pertanyaan + '</td>\
                  <td>' + value.jawaban + '</td>\
                  <td><button class="btn btn-danger hapusFAQ" data-id="' + index + '">Delete</button></td>\
              </tr>');
              }
              lastIndex = index;
          });
          $('#datapertanyaan').html(htmls);
      });

      //hapus FAQ
      // Remove Data
      $("body").on('click', '.hapusFAQ', function () {
        var id = $(this).attr('data-id');
        firebase.database().ref('informasi/faq/' + id).remove();
          // menampilkan alert
          alert("Berhasil menghapus data");
        });

      //insert pertanyaan
      $("#btnTambahfaq").click(function(){
          var pertanyaan = $("#pertanyaan").val();
          var jawaban = $("#jawaban").val();
          if(pertanyaan !=""&&jawaban!=""){
            var id = parseInt(lastIndex)+1
            firebase.database().ref("/informasi/faq/"+id).set({pertanyaan:pertanyaan,jawaban:jawaban}).then(function(){
              alert("Berhasil masukkan pertanyaan baru");
              lastIndex = id
            }).catch(function(error){
              alert(error);
            });
          }
          else{
            alert("Pastikan semua data terisi!");
          }
      });

      //=====================================================

        //set data di firebase
        $("#btndeskripsitoko").click(function(){
            var desc = $("#deskripsitoko").val();
            firebase.database().ref("/informasi/deskripsitoko").set(desc).then(function(){
              alert("Berhasil update informasi toko");
            }).catch(function(error){
              alert(error);
            });
        });

        $("#btndeskripsibahan").click(function(){
          var desc = $("#deskripsibahan").val();
          firebase.database().ref("/informasi/deskripsibahan").set(desc).then(function(){
            alert("Berhasil update informasi");
          }).catch(function(error){
            alert(error);
          });
      });
    });
});