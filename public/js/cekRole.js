$(document).ready(function(){
    firebase.auth().onAuthStateChanged(function(user) {
        //cek ada tidak yang login
        if(user!==null){
            //coba read database, kalau tdk punya permission, balik ke login
            firebase.database().ref('user/').once('value').then((snapshot)=>{
            }).catch((error)=>{
                $(location).attr('href', base_url+'/loginuser');
            });
        }
        else{
            //ini kalau tidak ada yang login
            $(location).attr('href', base_url+'/loginuser');
        }
    });
});