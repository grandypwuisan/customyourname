$(document).ready(function(){
    var formatter = new Intl.NumberFormat('in-ID', {
        style: 'currency',
        currency: 'IDR',
      });
    $('.modal').modal();
    firebase.auth().onAuthStateChanged(function(user) {
        //ambil data produk dari db dan tampilkan ke halaman daftarpesanan
        var id_pesanan = new URLSearchParams(window.location.search).get("id")
        ambildatapesanan(id_pesanan)
	});

    function ambildatapesanan(id){
        firebase.database().ref('pesanan/admin/'+id).once('value').then((snapshot)=>{
            var value = snapshot.val();
            var totalbayar = 0;
            var url_bukti=""
            if(snapshot){
                var htmls = "";
                var jumlah = [];
                var listbarang = [];
                url_bukti = value.url_bukti
               for(i=0; i<value.daftar_barang.length;i++){
                   var barang = value.daftar_barang[i]
                   listbarang.push(barang);
                   jumlah.push(barang.jumlah_produk);
                htmls=htmls+'<tr>\
                    <td><img class="gambarDetailProduk"src="'+barang.foto_produk+'"<br><br>' 
                    + barang.nama_produk + '<br>'+barang.jumlah_produk+' produk</td>'
                    +"<td> "+formatter.format(barang.harga_produk)+"</td>"
                    +"<td>"+barang.berat_produk+" gram</td>"
                    +'<td>'+ barang.warna_produk;
                    if(barang.kategori_produk=="kalung"){
                        htmls = htmls + ", "+barang.panjang_kalung;
                    }
                    var sub = barang.jumlah_produk*barang.harga_produk;
                    totalbayar = totalbayar+sub; 
                    htmls = htmls+'</td><td><span class="subtotal">'+formatter.format(sub)+'</span></td>';
                    if(barang.custom_produk =="ya"){
                        htmls=htmls+"<td><a id='"+i+"'href='#modal1'"
                        +"class='btnLihatDesain btn modal-trigger waves-effect waves-light pink lighten-2'>Lihat Desain</a></td>"
                    }
                    else{
                        htmls=htmls+"<td>bukan barang custom</td>"
                    }
               }
               $('#tabeldaftarbarang').html(htmls);

               //masukkan total harga yg harus dibayar
                $("#totalpembayaran").html(
                   "<b>Total Harga Barang: "+formatter.format(totalbayar)
                   +"<br>Total Pembayaran: "+formatter.format((parseInt(totalbayar)+parseInt(value.total_ongkir)))+"</b>"
                );

               //masukkan status pesanan
                if(value.status_pesanan=="baru"){
                    if(value.acc_admin=="tidak"){
                        var htmls2="<h5 style='color:red'>Status Pesanan: Menunggu Konfirmasi Admin</h5>"
                        +"<span>Dipesan : </span>"+value.tgl_pemesanan +"<hr>";
                        var htmls2=htmls2+'<br><br><br><textarea id="catatanAdmin" placeholder="Catatan Pesanan" class="materialize-textarea"></textarea>'
                        +'<a class="waves-effect waves-dark btn" id="btnTeruskan">Teruskan Pesanan</a><br><br>';
                    }
                    else{
                        var htmls2="<h5 style='color:red'>Status Pesanan: Menunggu Konfirmasi Pengrajin</h5>"
                        +"<span>Dipesan : </span>"+value.tgl_pemesanan +"<hr>";
                    }
                    $("#statuspesanan").html(htmls2);
                }
                else if (value.status_pesanan == "disetujui"){
                    var htmls2="<h5 style='color:red'>Status Pesanan: Telah disetujui, sedang menunggu pembayaran</h5>"
                    +"<span>Dipesan : </span>"+value.tgl_pemesanan +"<hr>"
                    $("#statuspesanan").html(htmls2);
                }
                else if (value.status_pesanan == "ditolak"){
                    var htmls2="<h5 style='color:red'>Status Pesanan: Ditolak</h5>"
                    +"<span>Dipesan : </span>"+value.tgl_pemesanan +"<hr>"; 
                    $("#statuspesanan").html(htmls2);
                }
                else if(value.status_pesanan=="dibayar"){
                    var htmls2="<h5 style='color:red'>Status Pesanan: Dibayar</h5>"
                    +"<span>Dipesan : </span>"+value.tgl_pemesanan +"<hr>"
                    +'<br><br><a href="#modal2" class="waves-effect waves-dark btn modal-trigger" id="btnLihatBukti">Lihat Bukti</a>&nbsp'
                    +'<br><br><br><textarea id="catatanPengrajin" placeholder="Catatan Pesanan" class="materialize-textarea"></textarea>'
                    +'<br><a class="waves-effect waves-dark btn" id="btnverif">Verifikasi Pesanan</a>&nbsp'
                     +'<a class="waves-effect waves-dark btn" id="tolakPesanan">Tolak Pesanan</a><br><br>';
                    $("#statuspesanan").html(htmls2);
                }
                else if (value.status_pesanan == "diverifikasi"){
                    var htmls2="<h5 style='color:red'>Status Pesanan: Telah diverifikasi, sedang menunggu pengrajin memproses</h5>"
                    +"<span>Dipesan : </span>"+value.tgl_pemesanan +"<hr>"
                    $("#statuspesanan").html(htmls2);
                }
                else if (value.status_pesanan == "diproses"){
                    var htmls2="<h5 style='color:red'>Status Pesanan: Sedang dalam proses pengerjaan, menunggu pengrajin mengirim pesanan</h5>"
                    +"<span>Dipesan : </span>"+value.tgl_pemesanan +"<hr>"
                    $("#statuspesanan").html(htmls2);
                }
                else if(value.status_pesanan=="dikirim"){
                    var htmls2="<h5 style='color:red'>Status Pesanan: Dikirim, jangan lupa memasukkan nomor resi jika telah tersedia</h5>"
                    +"<span>Dipesan : </span>"+value.tgl_pemesanan +"<hr>"
                    $("#statuspesanan").html(htmls2);
                }
                else if(value.status_pesanan=="selesai"){
                    var htmls2="<h5 style='color:red'>Status Pesanan: Selesai</h5>"
                    +"<span>Dipesan : </span>"+value.tgl_pemesanan +"<hr>"
                    $("#statuspesanan").html(htmls2);
                }

               //alamat pesanan
               var htmls3="<h6>Alamat Pengiriman:</h6>"
                    +value.pembeli.nama+"<br>"+value.pembeli.nomorhp+"<br>"+value.pembeli.alamat+", "
                    +value.pembeli.kecamatan+", "+value.pembeli.kota+", "+value.pembeli.provinsi
                    +"<br><br>"+"Kurir:   "+value.kurir
                    +"<br>Total Ongkir: "+formatter.format(value.total_ongkir)+"<br><b>Nomor resi:</b>";
                if(value.status_pesanan!="ditolak"&&value.status_pesanan!="selesai"){
                    htmls3 = htmls3+"<input style='width:100px; margin-left:1%;' id='nomorResi'type:text value='"+value.no_resi+"'></input>"
                    +'&nbsp&nbsp<a class="waves-effect waves-dark btn" id="btninputresi">Input Resi</a><br>';
                }
                else{
                    htmls3 = htmls3+"<span>"+value.no_resi+"</span>";
                }

                //catatan dari customer
                var htmls3=htmls3+"<b>Catatan Customer:</b><br>"+value.catatan+"<br><br>";
                $("#alamatpesanan").html(htmls3);
            }

            //input resi kalau status sudah terkirim
            $("#btninputresi").click(function(){
                if(value.status_pesanan=="dikirim"){
                     //baru bisa input resi
                     var noresi = $("#nomorResi").val()
                     var ref = firebase.database().ref("pesanan/admin/"+id);
                     var ref2 = firebase.database().ref("pesanan/"+value.pembeli.id_user+"/"+id)
                     ref.child("no_resi").set(noresi);
                     ref2.child("no_resi").set(noresi);
                     alert("nomor resi diupdate");
                }
                else{
                    alert("input nomor resi setelah melakukan pengiriman");
                }
            });

            $("#btnTeruskan").click(function(){
                var ref = firebase.database().ref("pesanan/admin/"+id);
                var ref2 = firebase.database().ref("pesanan/"+value.pembeli.id_user+"/"+id)
                var catatan = $("#catatanAdmin").val();
                var data = {
                    acc_admin:"ya",
                    catatan_admin : catatan
                }
                ref.update(data);
                ref2.update(data);
                alert("Pesanan telah diteruskan ke pengrajin");
                location.reload();
           });

            //lihat bukti pembayaran
            $("#btnLihatBukti").click(function(){
                var htmls="<img style='max-width:500px;max-height:500px'src='"+url_bukti+"'>";
                $("#buktitransfer").html(htmls);
            });

            //setujui pesanan
            $("#btnverif").click(function(){
                var ref = firebase.database().ref("pesanan/admin/"+id);
                var ref2 = firebase.database().ref("pesanan/"+value.pembeli.id_user+"/"+id)
                var catatan = $("#catatanPengrajin").val();
                var data = {
                    status_pesanan : "diverifikasi",
                    catatan : catatan
                }
                ref.update(data);
                ref2.update(data);
                alert("Pesanan telah diverifikasi");
                location.reload();
           });

           //tolak pesanan
           $("#tolakPesanan").click(function(){
               var catatan = $("#catatanPengrajin").val()
               if(catatan==""){
                   alert("Mohon isi catatan mengenai alasan penolakan");
               }
               else{
                   var ref = firebase.database().ref("pesanan/admin/"+id);
                   var ref2 = firebase.database().ref("pesanan/"+value.pembeli.id_user+"/"+id)
                   ref.child("status_pesanan").set("ditolak");
                   ref.child("catatan").set(catatan);
                   ref2.child("status_pesanan").set("ditolak");
                   ref2.child("catatan").set(catatan);
                   alert("Pesanan telah ditolak");
                   location.reload();
               }
           });

            $(".btnLihatDesain").click(function(){
                var index = $(this).attr('id');
                var desain = listbarang[index].desain;
                var htmls="";
                if(desain.jenis=="clipart"){
                    for(var i=0; i<desain.list_url_clipart.length; i++){
                        htmls=htmls+"<img src='"+desain.list_url_clipart[i]+"'>";
                    }
                }
                else{
                    htmls=htmls+"<img style='max-width:500px;max-height:500px'src='"+desain.url_gambar+"'>";
                }

                $("#desain").html(htmls);
            });

        }).catch((error)=>{
            console.log(error);
        });
    }

    function update_pesanan(idPesanan,indexBarang){
        alert("tes")
    }
});