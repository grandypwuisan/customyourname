$(document).ready(function(){
    $('.carousel').carousel();

    //ini untuk atasi bug double click dropdown
    $('select').material_select();
    document.querySelectorAll('.select-wrapper').forEach(t => t.addEventListener('click', e=>e.stopPropagation()));
   
    //ambil id
    var idProduk = new URLSearchParams(window.location.search).get("id");

    firebase.auth().onAuthStateChanged(function(user) {
        //ambil data dari paramter dan tampilkan di halamn detil barang
        //console.log(new URLSearchParams(window.location.search).get("tes"));
        var ref = firebase.database().ref();
        // Attach an asynchronous callback to read the data at our posts reference
        ref.child("produk").child(idProduk).once('value').then((snapshot) => {
        if (snapshot.exists()) {
            var data = snapshot.val();
            $("#namaproduk").val(data.namaproduk);
            $("#beratproduk").val(data.beratproduk);
            $("#hargaproduk").val(data.hargaproduk);
            $("#deskripsiproduk").val(data.deskripsiproduk);
            $("#kategoriproduk").find('option[value='+data.kategoriproduk+']').attr('selected','selected');
            $('#kategoriproduk').material_select();
            //ambil gambar dari storage
            var storage = firebase.storage();
            var storageRef = storage.ref();
            storageRef.child("produk").child(idProduk).listAll().then(function(res)                             {
                // Once we have the download URL, we set it to our img element
                res.items.forEach(function(imageRef) {
                    tampilkanGambar(imageRef);
                });
            }).catch(function(error) {
                // If anything goes wrong while getting the download URL, log the error
                console.error("Gagal mengambil data gambar "+error);
            });
        } else {
            console.log("No data available");
        }
        }).catch((error) => {
            console.error(error);
        });

        function tampilkanGambar(imageRef){
            imageRef.getDownloadURL().then(function(url){
                var html = `<a class='carousel-item'>`;
                //html = html+`<div onclick="hapusGambar(1)" class='btnDeleteFotoDetailproduk'>Delete</div>`;
                html = html+`<img class='gambarproduk' src='`+url+`'></a>`;
                $(".carousel").append(html);
                $('.carousel').removeClass('initialized');
                $('.carousel').carousel();
            }).catch(function(error){
                console.log(error);
            });
        }

        function hapusGambar(refGambar){
            alert("tes");
        }

        $("#btnUpdateProduk").click(function(){
          var nama = $("#namaproduk").val();
          var data={
            namaproduk : $("#namaproduk").val(),
            deskripsiproduk : $("#deskripsiproduk").val(),
            kategoriproduk : $("#kategoriproduk").val(),
            hargaproduk : $("#hargaproduk").val(),
            beratproduk : $("#beratproduk").val()
          };
          firebase.database().ref("/produk/"+idProduk).update(data).then(function(){
            alert("Berhasil Update Data Produk");
            //upload gambar
            var imageFiles = $("#fotoProduk")[0].files;
            if(imageFiles.length>0){
                for(var i=0; i<imageFiles.length; i++){
                    uploadImage(imageFiles[i],idProduk);
                }
            }
          }).catch(function(error){
            alert(error);
          });
        });

        //promise untuk upload foto produk
        function uploadImage(imageFile,produkID) {
            return new Promise(function (resolve, reject) {
                var rootRef = firebase.storage().ref();
                var imageRef = rootRef.child('produk');
                console.log(produkID.toString());
                var storageRef = imageRef.child('/'+produkID+'/'+imageFile.name)
                //Upload file
                var task = storageRef.put(imageFile);
                //Update progress bar
                task.on('state_changed',
                    function progress(snapshot){
                        var percentage = snapshot.bytesTransferred / snapshot.totalBytes * 100;
                    },
                    function error(err){
                        console.log(error);
                    },
                    function complete(){
                        task.snapshot.ref.getDownloadURL().then((url)=>{
                            var html = `<a class='carousel-item'>
                            <div onclick="hapusGambar(1)" class='btnDeleteFotoDetailproduk'>Delete
                            </div>
                            <img class='gambarproduk' src='`+url+`'>
                            </a>`;
                            $(".carousel").append(html);
                            $('.carousel').removeClass('initialized');
                            $('.carousel').carousel();
                        });
                    }
                );
            });
        }

    });
});