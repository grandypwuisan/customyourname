<!-- Admin Pass:admin@admin -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Customyournameid | Login</title>
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" href='style/style1.css'>
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col s12">
                <form class="formlogin" action="#">
                    <p id="login-title">customyourname.id</p>
                    <span style="font-size: x-large;">Sign in</span><br><br>
                    <input placeholder="Email" id="loginEmail" type="email" class="validate">
                    <input placeholder="Password" id="loginPassword" type="password" class="validate"><br><br>
                    <button style="width:100%;"class="btnLogin btn pink lighten-2 waves-effect waves-light" type="submit" name="action">Submit</button>
                </form>
            </div>
        </div>
    </div>
    <script src="https://www.gstatic.com/firebasejs/8.4.3/firebase.js"></script>
    <script pt src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="/js/initFirebase.js"></script>
    <script src="/js/script-loginuser.js"></script>
    <script>var base_url = '<?php echo base_url() ?>';</script>
</body>
</html>