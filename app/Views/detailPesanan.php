<!DOCTYPE html>
<html lang="en">
  <!--================================================================================
	Item Name: Materialize - Material Design Admin Template
	Version: 4.0
	Author: PIXINVENT
	Author URL: https://themeforest.net/user/pixinvent/portfolio
  ================================================================================ -->
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
    <title>Customyourname | Detail Transaksi</title>
    <!-- Favicons-->
    <link rel="icon" href="images/favicon/favicon-32x32.png" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-152x152.png">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
    <!-- For Windows Phone -->
    <!-- CORE CSS-->
    <link href="css//materialize.css" type="text/css" rel="stylesheet">
    <link href="css//style.css" type="text/css" rel="stylesheet">
    <!-- Custome CSS-->
    <link href="css/custom/custom.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href='style/style1.css'>
    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="vendors/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet">
    <link href="vendors/flag-icon/css/flag-icon.min.css" type="text/css" rel="stylesheet">
  </head>
  <body>
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
      <div id="loader"></div>
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START HEADER -->
    <header id="header" class="page-topbar">
      <!-- start header nav-->
      <div class="navbar-fixed">
        <nav class="navbar-color gradient-45deg-light-pink">
          <div class="nav-wrapper">
            <ul class="left">
              <li>
                <h1 class="logo-wrapper">
                  <a href="index.html" class="brand-logo darken-1">
                    <img src="images/logo/materialize-logo.svg" alt="materialize logo">
                    <span class="logo-text hide-on-med-and-down" style="font-size: 20px;">Customyourname.id</span>
                  </a>
                </h1>
              </li>
            </ul>
            <ul class="right hide-on-med-and-down">
              <li>
                <a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown">
                  <i class="material-icons">notifications_none
                    <small class="notification-badge pink accent-2">0</small>
                  </i>
                </a>
              </li>
              <li>
                <a href="javascript:void(0);" class="waves-effect waves-block waves-light profile-button" data-activates="profile-dropdown">
                  <span class="avatar-status avatar-online">
                    <img src="images/avatar/avatar-7.png" alt="avatar">
                    <i></i>
                  </span>
                </a>
              </li>
            </ul>
            <!-- notifications-dropdown -->
            <ul id="notifications-dropdown" class="dropdown-content">
              <li>
                <h6>NOTIFICATIONS
                  <span class="new badge"></span>
                </h6>
              </li>
            </ul>
            <!-- profile-dropdown -->
            <ul id="profile-dropdown" class="dropdown-content">
              <!-- <li>
                <a href="#" class="grey-text text-darken-1">
                  <i class="material-icons">face</i> Profile</a>
              </li> -->
              <li>
                <a class="logout"href="#" class="grey-text text-darken-1">
                  <i class="material-icons">keyboard_tab</i> Logout</a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
      <!-- end header nav-->
    </header>
    <!-- END HEADER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START MAIN -->
    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
         <!-- START LEFT SIDEBAR NAV-->
         <aside id="left-sidebar-nav">
          <ul id="slide-out" class="side-nav fixed leftside-navigation">
            <li class="user-details cyan darken-2">
              <div class="row">
                <div class="col col s8 m8 l8">
                  <ul id="profile-dropdown-nav" class="dropdown-content">
                    <li>
                      <a href="#" class="grey-text text-darken-1">
                        <i class="material-icons">face</i> Profile</a>
                    </li>
                    <li>
                      <a href="#" class="grey-text text-darken-1">
                        <i class="material-icons">keyboard_tab</i> Logout</a>
                    </li>
                  </ul>
                  <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown-nav">
                    <a style="color:white" id="namauserlogin">John Doe</a><i class="mdi-navigation-arrow-drop-down right"></i></a>
                  <p class="user-roal">Administrator</p>
                </div>
              </div>
            </li>
            <li class="no-padding">
              <ul class="collapsible" data-collapsible="accordion">
                <li class="bold active ">
                  <a href="<?=base_url('home')?>" class="waves-effect waves-cyan">
                      <span class="nav-text">Daftar Pesanan</span>
                    </a>
                </li>
                <li class="bold">
                  <a href="<?=base_url('daftarproduk')?>" class="waves-effect waves-cyan">
                      <span class="nav-text">Daftar Produk</span>
                    </a>
                </li>
                <li class="bold">
                  <a href="<?=base_url('tambahproduk')?>" class="waves-effect waves-cyan">
                      <span class="nav-text">Tambah Produk Baru</span>
                    </a>
                </li>
                <li class="bold">
                  <a href="<?=base_url('kotakpesan')?>" class="waves-effect waves-cyan">
                      <span class="nav-text">Kotak Pesan</span>
                    </a>
                </li>
                <li class="bold">
                  <a href="<?=base_url('daftaruser')?>" class="waves-effect waves-cyan">
                      <span class="nav-text">Daftar User</span>
                    </a>
                </li>
                <li class="bold">
                  <a href="<?=base_url('laporan')?>" class="waves-effect waves-cyan">
                      <span class="nav-text">Laporan</span>
                    </a>
                </li>
                <li class="bold">
                  <a href="<?=base_url('editinformasi')?>" class="waves-effect waves-cyan">
                      <span class="nav-text">Edit Informasi</span>
                    </a>
                </li>
              </ul>
            </li>
          </ul>
          <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only">
            <i class="material-icons">menu</i>
          </a>
        </aside>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->
        <section id="content">
          <!--start container-->
          <div class="container">
            <div class="row">
              <div class="col s10" id="statuspesanan"></div>
            </div>
            <div class="row">
              <div class="col s10" id="alamatpesanan"></div>
            </div>
            <div class="row" id="daftarbarang">
                <div class="col s10">
                  <!-- modal tampilkan desain -->
                  <div id="modal1" class="modal">
                    <div class="modal-content">
                      <center>
                      <h5>DESAIN</h5>
                      <div style="margin-top:3%"id="desain"></div>
                      </center>
                    </div>
                    <div class="modal-footer">
                      <a href="#!" class="modal-close waves-effect waves-green btn-flat">OK</a>
                    </div>
                  </div>
                  <!-- modal tampilkan bukti -->
                  <div id="modal2" class="modal">
                    <div class="modal-content">
                      <center>
                      <h5>Bukti Transfer</h5>
                      <div style="margin-top:3%"id="buktitransfer"></div>
                      </center>
                    </div>
                    <div class="modal-footer">
                      <a href="#!" class="modal-close waves-effect waves-green btn-flat">OK</a>
                    </div>
                  </div>
                    <table>
                        <thead>
                            <tr>
                                <td>Barang</td>
                                <td>Harga</td>
                                <td>Berat</td>
                                <td>Detail</td>
                                <td>Total Harga</td>
                                <td>Desain</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody id="tabeldaftarbarang">
                        <!-- data barang disini -->
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <h6 id="totalpembayaran"></h6>
            </div>
          </div>
          <!--end container-->
        </section>
        <!-- END CONTENT -->
      </div>
      <!-- END WRAPPER -->
    </div>
    <!-- END MAIN -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- ================================================
    Scripts
    ================================================ -->
    <!-- jQuery Library -->
    <script type="text/javascript" src="vendors/jquery-3.2.1.min.js"></script>
    <!--materialize js-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="js/plugins.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script src="https://www.gstatic.com/firebasejs/8.4.3/firebase.js"></script>
    <script type="text/javascript" src="js/initFirebase.js"></script>
    <script type="text/javascript" src="js/script-logout.js"></script>
    <script type="text/javascript" src="js/cekRole.js"></script>
    <script type="text/javascript" src="js/script-setnamauserlogin.js"></script>
    <script type="text/javascript" src="js/script-notif.js"></script>
    <script type="text/javascript" src="js/script-detailPesanan.js"></script>
    <script>var base_url = '<?php echo base_url() ?>';</script>
  </body>
</html>