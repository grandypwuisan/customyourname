<!DOCTYPE html>
<html lang="en">
  <!--================================================================================
	Item Name: Materialize - Material Design Admin Template
	Version: 4.0
	Author: PIXINVENT
	Author URL: https://themeforest.net/user/pixinvent/portfolio
  ================================================================================ -->
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
    <title>Customyourname | Pengrajin</title>
    <!-- Favicons-->
    <link rel="icon" href="images/favicon/favicon-32x32.png" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-152x152.png">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
    <!-- For Windows Phone -->
    <!-- CORE CSS-->
    <link href="css//materialize.css" type="text/css" rel="stylesheet">
    <link href="css//style.css" type="text/css" rel="stylesheet">
    <!-- Custome CSS-->
    <link href="css/custom/custom.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href='style/style1.css'>
    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="vendors/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet">
    <link href="vendors/flag-icon/css/flag-icon.min.css" type="text/css" rel="stylesheet">
  </head>
  <body>
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
      <div id="loader"></div>
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START HEADER -->
    <header id="header" class="page-topbar">
      <!-- start header nav-->
      <div class="navbar-fixed">
        <nav class="navbar-color gradient-45deg-light-pink">
          <div class="nav-wrapper">
            <ul class="left">
              <li>
                <h1 class="logo-wrapper">
                  <a href="index.html" class="brand-logo darken-1">
                    <img src="images/logo/materialize-logo.svg" alt="materialize logo">
                    <span class="logo-text hide-on-med-and-down" style="font-size: 20px;">Customyourname.id | Pengrajin</span>
                  </a>
                </h1>
              </li>
            </ul>
            <ul class="right hide-on-med-and-down">
              <li>
                <a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown">
                  <i class="material-icons">notifications_none
                    <small class="notification-badge pink accent-2">0</small>
                  </i>
                </a>
              </li>
              <li>
                <a href="javascript:void(0);" class="waves-effect waves-block waves-light profile-button" data-activates="profile-dropdown">
                  <span class="avatar-status avatar-online">
                    <img src="images/avatar/avatar-7.png" alt="avatar">
                    <i></i>
                  </span>
                </a>
              </li>
            </ul>
            <!-- notifications-dropdown -->
            <ul id="notifications-dropdown" class="dropdown-content">
              <li>
                <h6>NOTIFICATIONS
                  <span class="new badge"></span>
                </h6>
              </li>
            </ul>
            <!-- profile-dropdown -->
            <ul id="profile-dropdown" class="dropdown-content">
              <!-- <li>
                <a href="#" class="grey-text text-darken-1">
                  <i class="material-icons">face</i> Profile</a>
              </li> -->
              <li>
                <a class="logout"href="#" class="grey-text text-darken-1">
                  <i class="material-icons">keyboard_tab</i> Logout</a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
      <!-- end header nav-->
    </header>
    <!-- END HEADER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START MAIN -->
    <div id="main" style="padding-left: 5%;">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->
        <section id="content">
          <!--start container-->
          <div class="container">
              <!--card stats start-->
            <div id="card-stats">
              <div class="row mt-1">
                <div class="col s12 m6 l3">
                  <div class="card gradient-45deg-amber-amber gradient-shadow min-height-100 white-text" id="btnbaru">
                    <div class="padding-4">
                      <div class="col s7 m7">
                        <i class="material-icons background-round mt-5">fiber_new</i>
                        <p>Pesanan Baru</p>
                      </div>
                      <div class="col s5 m5 right-align">
                        <h2 id="baru"></h2>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col s12 m6 l3">
                  <div class="card gradient-45deg-green-teal gradient-shadow min-height-100 white-text" id="btndisetujui">
                    <div class="padding-4">
                      <div class="col s7 m7">
                        <i class="material-icons background-round mt-5">check</i>
                        <p>Disetujui Pengrajin</p>
                      </div>
                      <div class="col s5 m5 right-align">
                        <h2 id="disetujui"></h2>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col s12 m6 l3">
                  <div class="card gradient-45deg-purple-light-blue gradient-shadow min-height-100 white-text" id="btndibayar">
                    <div class="padding-4">
                      <div class="col s7 m7">
                        <i class="material-icons background-round mt-5">payment</i>
                        <p>Dibayar</p>
                      </div>
                      <div class="col s5 m5 right-align">
                        <h2 id="dibayar"></h2>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col s12 m6 l3">
                  <div class="card gradient-45deg-indigo-light-blue gradient-shadow min-height-100 white-text" id="btnVerifikasi">
                    <div class="padding-4">
                      <div class="col s7 m7">
                        <i class="material-icons background-round mt-5">verified_user</i>
                        <p>Diverifikasi</p>
                      </div>
                      <div class="col s5 m5 right-align">
                        <h2 id="diverifikasi"></h2>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- card selesai -->
            <!--card stats start-->
            <div id="card-stats">
              <div class="row mt-1">
                <div class="col s12 m6 l3">
                  <div class="card gradient-45deg-brown-brown gradient-shadow min-height-100 white-text"id="btndiproses">
                    <div class="padding-4">
                      <div class="col s7 m7">
                        <i class="material-icons background-round mt-5">cached</i>
                        <p>Dalam Proses</p>
                      </div>
                      <div class="col s5 m5 right-align">
                        <h2 id="diproses"></h2>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col s12 m6 l3">
                  <div class="card gradient-45deg-indigo-blue gradient-shadow min-height-100 white-text"id="btndikirim">
                    <div class="padding-4">
                      <div class="col s7 m7">
                        <i class="material-icons background-round mt-5">local_shipping</i>
                        <p>Dikirim</p>
                      </div>
                      <div class="col s5 m5 right-align">
                        <h2 id="dikirim"></h2>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col s12 m6 l3">
                  <div class="card gradient-45deg-green-teal gradient-shadow min-height-100 white-text"id="btnselesai">
                    <div class="padding-4">
                      <div class="col s7 m7">
                        <i class="material-icons background-round mt-5">done_all</i>
                        <p>Selesai</p>
                      </div>
                      <div class="col s5 m5 right-align">
                        <h2 id="selesai"></h2>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col s12 m6 l3">
                  <div class="card gradient-45deg-red-pink gradient-shadow min-height-100 white-text"id="btnditolak">
                    <div class="padding-4">
                      <div class="col s7 m7">
                        <i class="material-icons background-round mt-5">cancel</i>
                        <p>Dibatalkan</p>
                      </div>
                      <div class="col s5 m5 right-align">
                        <h2 id='ditolak'></h2>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- card selesai 2 -->
            <div class="row containerFilterPesanan">
            <div class="col s7" style="padding-top: 3%; padding-bottom:2%; color:#ff4081"><h4 id="statuspesanan">Pesanan Baru</h4></div>
              <!-- button filter pesanan berdasarkan status -->
              <!-- <a class="waves-effect waves-light btn filter_pesanan" id="baru">Pesanan Baru</a>
              <a class="waves-effect waves-light btn filter_pesanan"id="disetujui">Disetujui</a>
              <a class="waves-effect waves-light btn filter_pesanan"id="diverifikasi">Diverifikasi</a>
              <a class="waves-effect waves-light btn filter_pesanan"id="diproses">Dalam Proses</a>
              <a class="waves-effect waves-light btn filter_pesanan"id="dikirim">Terkirim</a>
              <a class="waves-effect waves-light btn filter_pesanan"id="selesai">Selesai</a>
              <a class="waves-effect waves-light btn filter_pesanan"id="ditolak">Batal</a> -->
            </div>
            <div class="row">
                <div class="col s12" id="daftarpesanan">
                    <!-- ini tempat daftar pesanan -->
                    <table>
                        <thead>
                            <tr>
                                <td>Pesanan</td>
                                <td>Pembeli</td>
                                <td>Status</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody id="tabeldaftarpesanan">
                        <!-- data barang disini -->
                        <!-- <h5 id="pilihpesanan"style="color:red">Silahkan pilih status pesanan</h5> -->
                        </tbody>
                    </table>
                </div>
            </div>
          </div>
          <!--end container-->
        </section>
        <!-- END CONTENT -->
      </div>
      <!-- END WRAPPER -->
    </div>
    <!-- END MAIN -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- ================================================
    Scripts
    ================================================ -->
    <!-- jQuery Library -->
    <script type="text/javascript" src="vendors/jquery-3.2.1.min.js"></script>
    <!--materialize js-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="js/plugins.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script src="https://www.gstatic.com/firebasejs/8.4.3/firebase.js"></script>
    <script type="text/javascript" src="js/initFirebase.js"></script>
    <script type="text/javascript" src="js/script-logout.js"></script>
    <script type="text/javascript" src="js/cekRolePengrajin.js"></script>
    <script type="text/javascript" src="js/script_pengrajin_daftarpesanan.js"></script>
    <script type="text/javascript" src="js/script-notif-pengrajin.js"></script>
    <script>var base_url = '<?php echo base_url() ?>';</script>
  </body>
</html>