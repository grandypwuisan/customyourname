<?php

namespace App\Controllers;
use TCPDF;

class customyourname extends BaseController
{
	public function index()
	{
		return view('welcome_message');
	}
	public function tes1()
	{
		return view("tes1");
	}
	public function login()
	{
		return view("login");
	}
	public function home(){
		return view("home");
	}
	public function tambahproduk(){
		return view("tambahproduk");
	}
	public function daftaruser(){
		return view("daftaruser");
	}
	public function daftarproduk(){
		return view("daftarproduk");
	}
	public function detailproduk(){
		return view("detailproduk");
	}
	public function editinformasi(){
		return view("editinformasi.php");
	}
	public function faq(){
		return view("daftarfaq");
	}
	public function kotakpesan(){
		return view("message");
	}
	public function detailpesanan(){
		return view("detailPesanan");
	}

	public function laporantransaksi(){
		return view('laporantransaksi');
	}
	public function laporandesain(){
		return view('laporandesain');
	}
	public function laporanpemesan(){
		return view('laporanpemesan');
	}
	public function laporanpesananditolak(){
		return view('laporanpesananditolak');
	}
	public function laporanunitproduksi(){
		return view('laporanunitproduksi');
	}

	public function printlaporanpesananditolak(){
		$datapesanan = $this->request->getPost('tabelnya');
		$tglawal = $this->request->getPost('tglAwal');
		$tglakhir = $this->request->getPost('tglAkhir');
		$tglawal_create = date_create($tglawal);
		$tglakhir_create = date_create($tglakhir);
		$tglawal_format = date_format($tglawal_create,"d F Y");
		$tglakhir_format = date_format($tglakhir_create,"d F Y");

		$json = json_decode($datapesanan);

		$pdf = new MYPDF('L', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->SetHeaderMargin(30);
        $pdf->SetTopMargin(60);
		$pdf->setFooterMargin(30);
        $pdf->AddPage('L', 'mm', 'A4');
        $pdf->SetFont('', 'B', 14);
        $pdf->Cell(277, 8, "Laporan Transaksi Gagal", 0, 1, 'C');
		$pdf->Cell(277, 10, "Periode ".$tglawal_format." Sampai ".$tglakhir_format, 0, 1, 'C');
		$pdf->Ln(10);
        $pdf->SetAutoPageBreak(true, 25);
		$html='<table bgcolor="#666666" border="1" cellpadding="2">
						<thead>
                        <tr bgcolor="#ffffff">
                            <th align="center">No</th>
                            <th  align="center">Tanggal</th>
                            <th  align="center">Pembeli</th>
                            <th  align="center">Catatan</th>
                        </tr></thead><tbody>';
						$i=0;
            foreach ($json as $key=>$data) 
                {
                    $i++;
                    $html.='<tr bgcolor="#ffffff" align="center">
                            <td align="center">'.$i.'</td>
                            <td>'.$data->tgl_pemesanan.'</td>
							<td>'.$data->pembeli->nama.'-'.$data->pembeli->kota.'<br>'
							.$data->pembeli->alamat.'</td>
                            <td align="center">'.$data->catatan.'</td>
                        </tr>';
                }
            $html.='</tbody></table>
			<h4>Total transaksi yang tidak berhasil pada periode ini adalah sebanyak '
			.$i
			.' transaksi';
            $pdf->writeHTML($html, true, false, true, false, '');
		$nama = "Laporan Pesanan Gagal Periode ".$tglawal." sampai ".$tglakhir.".pdf";
        $pdf->Output($nama,'D');
	}

	public function printlaporantransaksi(){
		$datapesanan = $this->request->getPost('tabelnya');
		$tglawal = $this->request->getPost('tglAwal');
		$tglakhir = $this->request->getPost('tglAkhir');
		$tglawal_create = date_create($tglawal);
		$tglakhir_create = date_create($tglakhir);
		$tglawal_format = date_format($tglawal_create,"d F Y");
		$tglakhir_format = date_format($tglakhir_create,"d F Y");

		$json = json_decode($datapesanan);

		$pdf = new MYPDF('L', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->SetHeaderMargin(30);
        $pdf->SetTopMargin(60);
		$pdf->setFooterMargin(30);
        $pdf->AddPage('L', 'mm', 'A4');
        $pdf->SetFont('', 'B', 14);
        $pdf->Cell(277, 8, "Laporan Transaksi", 0, 1, 'C');
		$pdf->Cell(277, 10, "Periode ".$tglawal_format." Sampai ".$tglakhir_format, 0, 1, 'C');
		$pdf->Ln(10);
        $pdf->SetAutoPageBreak(true, 25);
        // Add Header
        // $pdf->Ln(10);
        // $pdf->SetFont('', 'B', 12);
        // $pdf->Cell(20, 8, "No", 1, 0, 'C');
        // $pdf->Cell(60, 8, "Tanggal", 1, 0, 'C');
        // $pdf->Cell(100, 8, "Pembeli", 1, 0, 'C');
        // $pdf->Cell(37, 8, "Total Harga", 1, 0, 'C');
		// $pdf->Cell(50, 8, "Ongkir", 1, 1, 'C');php 
        // $pdf->SetFont('', '', 12);
        // $no=0;
        // foreach ($json as $key=>$data){
        //     $no++;
        //     $pdf->Cell(20,8,$no,1,0, 'C');
		// 	$pdf->Cell(60,8,$data->tgl_pemesanan,1,0);
        //     $pdf->Cell(100,8,$data->pembeli->nama,1,0);
        //     $pdf->Cell(37,8,$data->total_harga,1,0);
		// 	$pdf->Cell(50,8,$data->total_ongkir,1,0);
		// 	$pdf->Ln();
        // }
		// foreach ($json as $key=>$data){
        //     $no++;
        //     $pdf->Cell(20,8,$no,1,0, 'C');
		// 	$pdf->Cell(60,8,$data->tgl_pemesanan,1,0);
        //     $pdf->Cell(100,8,$data->pembeli->nama,1,0);
        //     $pdf->Cell(37,8,$data->total_harga,1,0);
		// 	$pdf->Cell(50,8,$data->total_ongkir,1,0);
		// 	$pdf->Ln();
        // }
		$html='<table bgcolor="#666666" border="1" cellpadding="2">
						<thead>
                        <tr bgcolor="#ffffff">
                            <th align="center">No</th>
                            <th  align="center">Tanggal</th>
                            <th  align="center">Pembeli</th>
                            <th  align="center">Total Harga</th>
							<th  align="center">Ongkir</th>
                        </tr></thead><tbody>';
						$i=0;
						$total=0;
            foreach ($json as $key=>$data) 
                {
                    $i++;
                    $html.='<tr bgcolor="#ffffff" align="center">
                            <td align="center">'.$i.'</td>
                            <td>'.$data->tgl_pemesanan.'</td>
							<td>'.$data->pembeli->nama.'-'.$data->pembeli->kota.'<br>'
							.$data->pembeli->alamat.'</td>
                            <td align="center">Rp '.number_format($data->total_harga,0,",",",").'</td>
                            <td align="center">Rp '.number_format($data->total_ongkir,0,",",",").'</td>
                        </tr>';
					$total = $total+$data->total_harga;
                }
            $html.='</tbody></table>
			<h4>Total pendapatan pada periode ini sebesar Rp'
			.number_format($total,0,",",",")
			.' (tidak termasuk ongkir)</h4>';
            $pdf->writeHTML($html, true, false, true, false, '');
		$nama = "Laporan Pemesanan Periode ".$tglawal." sampai ".$tglakhir.".pdf";
        $pdf->Output($nama,'D');
	}

	public function printlaporanunitproduksi(){
		$datapesanan = $this->request->getPost('tabelnya');
		$tglawal = $this->request->getPost('tglAwal');
		$tglakhir = $this->request->getPost('tglAkhir');
		$tglawal_create = date_create($tglawal);
		$tglakhir_create = date_create($tglakhir);
		$tglawal_format = date_format($tglawal_create,"d F Y");
		$tglakhir_format = date_format($tglakhir_create,"d F Y");

		$json = json_decode($datapesanan);

		$pdf = new MYPDF('L', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->SetHeaderMargin(30);
        $pdf->SetTopMargin(60);
		$pdf->setFooterMargin(30);
        $pdf->AddPage('L', 'mm', 'A4');
        $pdf->SetFont('', 'B', 14);
        $pdf->Cell(277, 8, "Laporan Unit Produksi", 0, 1, 'C');
		$pdf->Cell(277, 10, "Periode ".$tglawal_format." Sampai ".$tglakhir_format, 0, 1, 'C');
		$pdf->Ln(10);
        $pdf->SetAutoPageBreak(true, 25);
		$html='<table bgcolor="#666666" border="1" cellpadding="2">
						<thead>
                        <tr bgcolor="#ffffff">
                            <th width="7%" align="center">No</th>
                            <th width="43%" align="center">Produk</th>
                            <th width="25%" align="center">Kategori</th>
                            <th width="25%" align="center">Jumlah Terjual</th>
                        </tr></thead><tbody>';
						$i=0;
			$jumlahkalung =0;
			$jumlahgelang=0;
			$jumlahcincin=0;
			$jumlahanting=0;
            foreach ($json as $key=>$data) 
                {
                    $i++;
                    $html.='<tr bgcolor="#ffffff" align="center">
                            <td width="7%"  align="center">'.$i.'</td>
                            <td width="43%" >'.$data->nama.'</td>
							<td width="25%" >'.$data->kategori.'</td>
                            <td width="25%" align="center">'.$data->jumlah.' produk</td>
                        </tr>';
					if($data->kategori=="kalung"){
						$jumlahkalung = $jumlahkalung+($data->jumlah);
					}
					else if($data->kategori=="gelang"){
						$jumlahgelang = $jumlahgelang+($data->jumlah);
					}
					else if($data->kategori=="cincin"){
						$jumlahcincin=$jumlahcincin+($data->jumlah);
					}
					else{
						$jumlahanting=$jumlahanting+($data->jumlah);
					}
                }
            $html.='</tbody></table>
			<h4>
			Kalung terjual : '.$jumlahkalung.
			'<br>Gelang terjual : '.$jumlahgelang.
			'<br>Cincin terjual : '.$jumlahcincin.
			'<br>Anting terjual : '.$jumlahanting.
			'</h4>';
            $pdf->writeHTML($html, true, false, true, false, '');
		$nama = "Laporan Unit Produksi Periode ".$tglawal." sampai ".$tglakhir.".pdf";
        $pdf->Output($nama,'D');
	}

	public function laporan(){
		return view("menulaporan");
		// $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// $pdf->SetAuthor('Nicola Asuni');
		// $pdf->SetTitle('TCPDF Example 001');
		// $pdf->SetSubject('TCPDF Tutorial');

		// // set auto page breaks
		// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// $pdf->addPage();

		// // output the HTML content
		// $pdf->writeHTML($html, true, false, true, false, '');

		// $this->response->setContentType('application/pdf');
		// $pdf->Output('laporan.pdf', 'I');
	}
	//pengrajin
	public function home_pengrajin(){
		return view("home_pengrajin");
	}
	public function detailPesananPengrajin(){
		return view("detailPesananPengrajin");
	}
}
class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        // Logo
        $image_file = K_PATH_IMAGES.'headerlaporan.png';
        $this->Image($image_file, 50, 0, 200, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        // Set font
        $this->SetFont('helvetica', 'B', 20);
		$this->Ln(50);
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}


